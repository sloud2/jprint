from setuptools import setup, find_packages

setup(
    name = 'jprint',
    version = '0.0.2',
    author = 'sloud',
    url = 'https://bitbucket.org/sloud2/jprint',
    author_email = 'jeremydrewcloud@gmail.com',
    packages = ['jprint'],
    entry_points = {
        'console_scripts': [
            'jprint = jprint.jprint:main'
            ]
        }
    )
