#!/bin/env python3
'''
pyporg
[{}]

small script to make starting a python project faster. it parses a json file
and returns values for the provided key in a format that make bash processing
easier.
'''.format('v0.0.1')

import json
import argparse
import os
import sys
from keyword import kwlist
import re

class GenClass():
    ''' 
    dotdicts act similar to javascript objects. you can assign new
    attributes with the dot.new_attr = new_val. and non exsistant
    attributes simple return None when accessed. eg.
    
    dot.does_not_exist is None == True
    '''
    def __init__(self, dict_={}):
        self.add_dict(dict_)

    def __setattr__(self, name, val):
        # sanitize the name
        name = self._sanitize(name)
       
        if type(val) is dict:
            self.__dict__[name] = GenClass(val)
        elif type(val) is list:
            self.__dict__[name] = self._genlist(val)
        else:
            self.__dict__[name] = val

    def _genlist(self, list_):
        l = []
        for elem in list_:
            if type(elem) is dict:
                l.append(GenClass(elem))
            elif type(elem) is list:
                l.append(self._genlist(elem))
            else:
                l.append(elem)
        
        return l

    def __getitem__(self, name):
        return self.__dict__.get(name)

    def __setitem__(self, name, val):
        self.__setattr__(name, val)

    def _sanitize_keywords(self, name):
        return name if name not in kwlist else ''.join([name, '_'])

    def _sanitize_bad_chars(self, name):
        return '_'.join(re.split('[^_a-zA-Z0-9]+', name))

    def _sanitize(self, name):
        name = self._sanitize_bad_chars(name)
        name = self._sanitize_keywords(name)
        return name
        
    def add_dict(self, dict_):
        for name, val in dict_.items():
            self.__setattr__(name, val)
            
    def keys(self):
        return self.__dict__.keys()

    def __str__(self):
        return str(self.__dict__)


def safe_load(fin):
    
    try:
        with open(fin, mode='r') as jason:
            prog = json.load(jason)

    except FileNotFoundError as err:
        prog = {'error': 'FileNotFoundError',
                'trace': str(err)}

    except PermissionError as err:
        prog = {'error': 'PermissionError',
                'trace': str(err)}

    except json.decoder.JSONDecodeError as err:
        prog = {'error': 'JSONDecodeError',
                'trace': str(err)} 

    except IsADirectoryError as err:
        prog = {'error': 'IsADirectoryError',
                'trace': str(err)}

    return prog

def _jprint_list(jason, v=False, ind=2, lvl=0):
    if v:
        indent = ' '*lvl*ind
        jstr = '[\n'
        
        for elem in jason:
            jstr += '{}{}, \n'.format(indent+' '*ind, 
                                      _jprint(elem, v=v, lvl=lvl+1, ind=ind))
        
        jstr = jstr[0:-3] + '\n' + indent + ']'
    else:
        jstr = '\n'.join([str(elem) for elem in jason])
    return jstr

def _jprint_GenClass(jason, v=False, ind=2, lvl=0):
    jason = jason.__dict__

    if v:
        indent = ' '*lvl*ind
        jstr = '{\n'
        
        for key, val in jason.items():
            jstr += '{}{}: {}, \n'.format(indent+' '*ind, 
                                          repr(key), 
                                          _jprint(val, v=v, lvl=lvl+1, ind=ind))
        
        jstr = jstr[0:-3] + '\n' + indent + '}'
    else:
        jstr = '\n'.join([str(key) for key in jason.keys()])
    return jstr

def _jprint(jason, v=False, ind=2, lvl=0):
    if type(jason) is GenClass:
        jstr = _jprint_GenClass(jason, v=v, lvl=lvl, ind=ind)
    
    elif type(jason) is list:
        jstr = _jprint_list(jason, v=v, lvl=lvl, ind=ind)
    
    else:
        jstr = repr(jason) if v else str(jason)
    
    return jstr

def jprint(jason, file=sys.stdout, v=False, ind=2):
    print(_jprint(jason, v=v, ind=ind), file=file)

def sanitize(line):
    error = 'print("stop being bad")'

    if type(line) is not str:
        return error

    if line.count(';') > 0:
        return error

    if line.count('(') > 0:
        return error

    if line.count(')') > 0:
        return error

    if not line.startswith('root'):
        return error
    
    return line

def convert(obj):
    if type(obj) is list:
        return [ convert(elem) for elem in obj ]
    elif type(obj) is dict:
        return GenClass(obj);
    else:
        return obj

def main():
    # parse args
    parser = argparse.ArgumentParser(description='kickstart a python project')
    parser.add_argument('-j', help='the json kickstarted file')
    parser.add_argument('-v', help='verbose', action='store_true')
    parser.add_argument('-i', help='indent', type=int, default=3)
    parser.add_argument('-l', help='length', action='store_true')
    parser.add_argument('key', help=' request key\' value')
    args = parser.parse_args()

    root = safe_load('pyporg.json') if args.j is None else safe_load(args.j)

    # check for errors loading root
    if type(root) is not list and root.get('error') is not None:
        print('error: {}\ntrace:\n{}'.format(root['error'], root['trace'])) 
        sys.exit(1)
    
    # convert root
    root = convert(root)

    try:

        if args.l is True:
            print(eval('len('+sanitize(args.key)+')'))
        else:    
            val = eval(sanitize(args.key))
            jprint(val, v=args.v, ind=args.i)
    except Exception as err:
        print(err)

if __name__ == '__main__':
    main()
